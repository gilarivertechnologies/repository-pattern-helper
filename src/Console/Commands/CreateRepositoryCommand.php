<?php
/**
 * Command to create stubs for repositories
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Console\Commands;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts\RepositoryServiceContract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Contract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Model;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Repository;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Services\RepositoryService;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

/**
 * Class CreateRepositoryCommand
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Console\Commands
 */
class CreateRepositoryCommand extends Command
{
    /**
     * @var RepositoryServiceContract
     */
    protected $repositoryService;

    /**
     * @var string
     */
    protected $signature = 'larapi-helpers:repository:make {model}';

    /**
     * @var string
     */
    protected $description = 'Creates a repository';

    /**
     * CreateRepositoryCommand constructor.
     * @param RepositoryServiceContract $repositoryService
     */
    public function __construct(RepositoryServiceContract $repositoryService)
    {
        parent::__construct();

        $this->repositoryService = $repositoryService;
    }

    public function handle(): void
    {
        $modelName = sprintf('%s\\%s', RepositoryService::MODEL_ROOT_NAMESPACE, Str::studly($this->argument('model')));
        $model = new Model($modelName);

        $this->repositoryService->ensureModelExists($model);

        $contract = new Contract(sprintf('%s\\%sRepositoryContract', RepositoryService::CONTRACT_NAMESPACE, $model->getRelativeModel()));
        $repository = new Repository(sprintf('%s\\%sRepository', RepositoryService::REPOSITORY_NAMESPACE, $model->getRelativeModel()));

        $contract->setRepository($repository);
        $repository->setContract($contract);
        $repository->setModel($model);

        $this->repositoryService->createFileFromStubContents(
            $this->repositoryService->generateContractStub($contract),
            $contract->getFqnFilename()
        );

        $this->repositoryService->createFileFromStubContents(
            $this->repositoryService->generateRepositoryStub($repository),
            $repository->getFqnFilename()
        );

        $this->info(sprintf('Created Repository: %s', $repository->getFqnFilename()));
        $this->info(sprintf('Created Repository Contract: %s', $contract->getFqnFilename()));
        $this->info('You should ensure that everything looks correct in those files. Don\'t forget to write tests for new methods ;)');
    }
}
