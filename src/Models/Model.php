<?php
/**
 * Domain model for working with an existing model to create repository for
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Models;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Services\RepositoryService;

/**
 * Class Model
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Models
 */
class Model extends AbstractObjectModel
{
    /**
     * @return string
     */
    public function getRelativeModel(): string
    {
        return str_replace(sprintf('%s\\', RepositoryService::MODEL_ROOT_NAMESPACE), '', $this->getFqn());
    }

    /**
     * @return void
     */
    public function ensureExists(): void
    {
        if (!class_exists($this->getFqn())) {
            throw new \RuntimeException(sprintf('Model %s does not exist', $this->getFqn()));
        }
    }
}
