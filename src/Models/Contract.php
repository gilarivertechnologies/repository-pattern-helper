<?php
/**
 * Domain model for creating a contract
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Models;

/**
 * Class Contract
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Models
 */
class Contract extends AbstractObjectModel
{
    /**
     * @var Repository
     */
    protected $repository;

    /**
     * @param Repository $repository
     */
    public function setRepository(Repository $repository): void
    {
        $this->repository = $repository;
    }

    /**
     * @return Repository
     */
    public function getRepository(): Repository
    {
        return $this->repository;
    }
}
