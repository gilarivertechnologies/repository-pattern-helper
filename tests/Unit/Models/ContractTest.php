<?php
/**
 * Test for model Contract
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Models;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Contract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Repository;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\TestCase;

/**
 * Class ContractTest
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Models
 */
class ContractTest extends TestCase
{
    public function testNamespaceParsedCorrectly()
    {
        $contract = new Contract('App\\Contracts\\Repositories\\Contract');

        $this->assertSame('App\\Contracts\\Repositories', $contract->getNamespace());
        $this->assertSame('Contract', $contract->getName());
        $this->assertSame('App\\Contracts\\Repositories\\Contract', $contract->getFqn());
    }

    public function testFilesystemParsedCorrectly()
    {
        $contract = new Contract('App\\Contracts\\Repositories\\Contract');

        $this->assertSame('Contract.php', $contract->getFilename());
        $this->assertSame('Contracts/Repositories/Contract.php', $contract->getFqnFilename());
    }

    public function testSetGetRepository()
    {
        $contract = new Contract('App\\Contracts\\Repositories\\Contract');
        $repository = new Repository('App\\Repositories\\Repository');

        $contract->setRepository($repository);
        $this->assertSame($repository, $contract->getRepository());
    }
}
