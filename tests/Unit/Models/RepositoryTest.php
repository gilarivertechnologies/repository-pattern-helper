<?php
/**
 * Test for model Contract
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Models;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Contract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Model;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Repository;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\TestCase;

/**
 * Class RepositoryTest
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Models
 */
class RepositoryTest extends TestCase
{
    public function testNamespaceParsedCorrectly()
    {
        $repository = new Repository('App\\Repositories\\Repository');

        $this->assertSame('App\\Repositories', $repository->getNamespace());
        $this->assertSame('Repository', $repository->getName());
        $this->assertSame('App\\Repositories\\Repository', $repository->getFqn());
    }

    public function testFilesystemParsedCorrectly()
    {
        $repository = new Repository('App\\Repositories\\Repository');

        $this->assertSame('Repository.php', $repository->getFilename());
        $this->assertSame('Repositories/Repository.php', $repository->getFqnFilename());
    }

    public function testSetGetContract()
    {
        $repository = new Repository('App\\Repositories\\Repository');
        $contract = new Contract('App\\Contracts\\Repositories\\Contract');

        $repository->setContract($contract);
        $this->assertSame($contract, $repository->getContract());
    }

    public function testSetGetModel()
    {
        $repository = new Repository('App\\Repositories\\Repository');
        $model = new Model('App\\Models\\Model');

        $repository->setModel($model);
        $this->assertSame($model, $repository->getModel());
    }
}
