<?php
/**
 * Test for RepositoryService
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Services;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts\DirectoryUtilityContract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Contract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Model;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Repository;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Services\RepositoryService;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\TestCase;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;
use Mockery\MockInterface;

/**
 * Class RepositoryServiceTest
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Services
 */
class RepositoryServiceTest extends TestCase
{
    /**
     * @var string
     */
    protected $contractStub = 'Repository: {{REPOSITORY_NAME}}, Namespace: {{CONTRACT_NAMESPACE}}, Name: {{CONTRACT_NAME}}';

    /**
     * @var string
     */
    protected $repositoryStub = 'Name: {{REPOSITORY_NAME}}, Namespace: {{REPOSITORY_NAMESPACE}}, ContractFqn: {{CONTRACT_FQN}}, ModelFqn: {{MODEL_FQN}}, ContractName: {{CONTRACT_NAME}}, ModelName: {{MODEL_NAME}}';

    /**
     * @var string
     */
    protected $appPath = '/path/to/app';

    /**
     * @var RepositoryService
     */
    protected $service;

    /**
     * @var MockInterface
     */
    protected $filesystem;

    /**
     * @var MockInterface
     */
    protected $appMock;

    /**
     * @var MockInterface
     */
    protected $directoryUtility;

    /**
     * @var string
     */
    protected $stubPath = '/stubs';

    public function setUp()
    {
        parent::setUp();

        $this->filesystem = \Mockery::mock(Filesystem::class);
        $this->appMock = \Mockery::mock(Application::class);
        $this->directoryUtility = \Mockery::mock(DirectoryUtilityContract::class);

        $this->appMock->shouldReceive('path')->andReturn($this->appPath);

        $this->service = new RepositoryService($this->filesystem, $this->appMock, $this->directoryUtility, $this->stubPath);
    }

    public function testEnsureModelExistsSuccess()
    {
        $model = new Model(\Illuminate\Database\Eloquent\Model::class);
        $this->service->ensureModelExists($model);
        $this->assertTrue(true);
    }

    public function testEnsureModelExistsThrowsExceptionWhenDoesNotExist()
    {
        $this->expectException(\RuntimeException::class);

        $model = new Model('Path\\To\\Nonexistent\\Model');
        $this->service->ensureModelExists($model);
    }

    public function testGenerateContractStub()
    {
        $contract = new Contract('App\\Contracts\\Repositories\\RepositoryContract');
        $repository = new Repository('App\\Repositories\\Repository');

        $contract->setRepository($repository);

        $this->filesystem->shouldReceive('get')
            ->once()
            ->with($this->stubPath . '/contract.stub')
            ->andReturn($this->contractStub);

        $output = $this->service->generateContractStub($contract);
        $this->assertSame(
            'Repository: Repository, Namespace: App\\Contracts\\Repositories, Name: RepositoryContract',
            $output
        );
    }

    public function testGenerateRepositoryStub()
    {
        $model = new Model('App\\Models\\Model');
        $repository = new Repository('App\\Repositories\\Repository');
        $contract = new Contract('App\\Contracts\\Repositories\\RepositoryContract');

        $repository->setModel($model);
        $repository->setContract($contract);

        $this->filesystem->shouldReceive('get')
            ->once()
            ->with($this->stubPath . '/repository.stub')
            ->andReturn($this->repositoryStub);

        $output = $this->service->generateRepositoryStub($repository);
        $this->assertSame(
            'Name: Repository, Namespace: App\\Repositories, ContractFqn: App\\Contracts\\Repositories\\RepositoryContract, ModelFqn: App\\Models\\Model, ContractName: RepositoryContract, ModelName: Model',
            $output
        );
    }

    public function testCreateFileFromStubContents()
    {
        $stub = 'FileStubContents';
        $filename = 'path/to/file.php';

        $this->directoryUtility->shouldReceive('createDirectoryTreeIfNotExists')
            ->once()
            ->with('path/to/file.php');

        $this->filesystem->shouldReceive('put')
            ->once()
            ->with('/path/to/app/path/to/file.php', $stub);

        $this->service->createFileFromStubContents($stub, $filename);
    }
}
